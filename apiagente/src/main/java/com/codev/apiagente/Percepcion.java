package com.codev.apiagente;

public class Percepcion {

    private String name;
    private Razonamiento razonamiento;

    public Percepcion() {
    }

    public String setName(String name){
        this.name = name;
        razonamiento = new Razonamiento(name);
        return getGender();
    }

    public String getGender(){
        String gender;
        gender = razonamiento.getGender();
        return  gender;
    }
}
