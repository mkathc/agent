package com.codev.apiagente;

public class Razonamiento{

    private String name;
    private Accion accion;

    public Razonamiento(String name) {
        this.name = name;
        accion = new Accion(name);
    }

    public String getGender(){
        String gender;
        char lastLetter = getLastLetter(name);

        switch (lastLetter){
            case 'a':
                gender = accion.isWoman();
                break;
            case 'A':
                gender = accion.isWoman();
                break;
            case 'o':
                gender = accion.isMan();
                break;
            case 'O':
                gender = accion.isMan();
                break;
            default:
                gender = accion.isNone();
                break;
        }
        return gender;

    }

    private char getLastLetter(String name){
        char letter;
        letter = name.charAt(name.length()-1);
        return letter;
    }
}
