package com.codev.apiagente;

public class Accion {
    private String name;

    public Accion(String name) {
        this.name = name;
    }

    public String isMan(){
        return "Hola chico, " + name+"!";
    }

    public String isWoman(){
        return "Hola chica, " + name+"!";
    }

    public String isNone(){
        return "Hola, " + name+"!";
    }

}
